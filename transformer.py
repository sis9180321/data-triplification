import sys
import csv
from rdflib import Graph, Literal, RDF, URIRef, Namespace
from rdflib.namespace import XSD

# Define the SAREF namespace
SAREF = Namespace("https://saref.etsi.org/core/")
S4Bldg = Namespace("https://saref.etsi.org/saref4bldg/")
EX = Namespace("https://example.org/")

# Create a new RDF graph
g = Graph()
g.bind("saref", SAREF)
g.bind("s4bldg", S4Bldg)
g.bind("ex", EX)

csv_file_path = sys.argv[1]

# Read the Household Data dataset
with open(csv_file_path, 'r') as file:
    reader = csv.DictReader(file)
    for row in reader:
        timestamp = row['utc_timestamp']
        
        for key, value in row.items():
            if key == 'utc_timestamp' or key == 'cet_cest_timestamp':
                continue
            
            # Extract information about device from key
            key_parts = key.split('_')
            if len(key_parts) < 3:
                continue  # Skip keys that don't match the expected format
            device_type = key_parts[2]
            
            if value:  # Make sure the value is not empty
                # Create a unique URI for each measurement
                measurement_uri = EX[f"measurement/{timestamp}/{key}"]
                
                # Add triples to the graph
                g.add((measurement_uri, RDF.type, SAREF.Measurement))
                g.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
                g.add((measurement_uri, SAREF.hasValue, Literal(value, datatype=XSD.float)))
                
                # Add property type
                property_uri = EX[f"property/{key}"]
                g.add((measurement_uri, SAREF.relatesToProperty, property_uri))
                g.add((property_uri, RDF.type, SAREF.Property))
                
                # Add device type
                device_uri = EX[f"device/{device_type}"]
                g.add((device_uri, RDF.type, SAREF.Device))
                g.add((device_uri, SAREF.measuresProperty, property_uri))
                
                # Add room information (simplified for this example)
                room_uri = EX[f"room/{key_parts[1]}"]
                g.add((room_uri, RDF.type, S4Bldg.Room))
                g.add((device_uri, S4Bldg.isContainedIn, room_uri))

# Serialize the graph in Turtle format and save to a file
with open('graph.ttl', 'w') as f:
    f.write(g.serialize(format='turtle'))

print("Completion!")